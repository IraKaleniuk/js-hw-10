"use strict";

const tabs = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelector(".tabs-content");

for (let tab of tabs) {
    tab.addEventListener("click", onTabClick);
}

function onTabClick(event) {
    const activeTab = document.querySelector(".tabs .active");
    activeTab.classList.remove("active");
    const activeTabContent = document.querySelector(".tabs-content > .active");
    activeTabContent.classList.remove("active")

    event.target.classList.add("active");
    for (let tabContent of tabsContent.children) {
        if (tabContent.dataset.tabTitle === event.target.dataset.tabTitle) {
            tabContent.classList.add("active")
        }
    }
}










